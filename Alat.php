<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alat".
 *
 * @property integer $id
 * @property string $nama
 * @property string $jenis
 *
 */
class Alat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 20],
            [['jenis'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_alat' => 'ID',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
}
